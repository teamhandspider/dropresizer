﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using System;
/*using System.DrawingCore;
using System.DrawingCore.Imaging;*/
using System.IO;
using System.Linq;
using System.Threading;

namespace dropresizer
{
    class Program
    {
        static void Main(string[] args)
        {
            string monitorPath = "";
            if (args.Length > 0)
                monitorPath = args[0];

            else monitorPath = @"C:\dropresizer";

            Directory.CreateDirectory(monitorPath);

            Console.WriteLine("Monitoring: " + monitorPath);

            Monitoring(monitorPath);                    
        }

        static int widthPixels = 1024;
        static int quality = 90;

        private static void Monitoring(string monitorPath) {
            var dirInf = new DirectoryInfo(monitorPath);
            while(true) {
                var files = dirInf.GetFiles("*.jpg");

                foreach (var item in files) {
                    if(!item.Name.Contains("resized")) {
                        int tries = 0;
                        int maxAttempts = 10;
                        for (int i = 0; i < maxAttempts; i++) {
                            try {
                                ResizeItem(item);
                                break;
                            }
                            catch (System.Exception e) {
                                if(i == maxAttempts) {
                                    Console.WriteLine(e);
                                    Console.WriteLine();
                                    Console.WriteLine();
                                }
                                else {
                                    Console.WriteLine(e.Message);
                                }
                            }
                            tries++;                            
                            Thread.Sleep(3000);
                        }

                        Thread.Sleep(1000);
                        var dumpFold = new DirectoryInfo(Path.Combine(monitorPath, "processed"));
                        dumpFold.Create();
                        var movetopath = Path.Combine(dumpFold.FullName, item.Name);
                        if (File.Exists(movetopath)) File.Delete(movetopath);
                        item.MoveTo(movetopath);
                    }
                }
                Thread.Sleep(500);
            }
        }

        private static void ResizeItem(FileInfo item) {           

            /*using (var toResizeImage = new Bitmap(System.DrawingCore.Image.FromFile(item.FullName))) {
                var scale = (float)widthPixels / (float)toResizeImage.Width;
                var newW = scale * toResizeImage.Width;
                var newH = scale * toResizeImage.Height;
                Console.WriteLine($"{item.Name}: {toResizeImage.Width}x{toResizeImage.Height} -> {newW}x{newH}");

                using (var graphics = Graphics.FromImage(toResizeImage)) {
                    graphics.CompositingQuality = System.DrawingCore.Drawing2D.CompositingQuality.HighQuality;
                    graphics.InterpolationMode = System.DrawingCore.Drawing2D.InterpolationMode.HighQualityBicubic;
                    graphics.CompositingMode = System.DrawingCore.Drawing2D.CompositingMode.SourceCopy;
                    graphics.DrawImage(toResizeImage, 0, 0, newW, newH);
                    
                    using(var output = File.Open(GetOutputPath(item), FileMode.Create)) {
                        var encoderParams = new EncoderParameters(1);
                        encoderParams.Param[0] = new EncoderParameter(Encoder.Quality, quality);

                        var codec = ImageCodecInfo.GetImageDecoders().First(x => x.FormatID == ImageFormat.Jpeg.Guid);

                        toResizeImage.Save(output, codec, encoderParams);
                    }
                }
            }*/

            using(var input = File.Open(item.FullName,FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {                
                var image = Image.Load(input);

                var scale = (float)widthPixels / (float)image.Width;
                int newW = (int)(scale * image.Width);
                int newH = (int)(scale * image.Height);
                Console.WriteLine($"{item.Name}: {image.Width}x{image.Height} -> {newW}x{newH}");

                image.Mutate(x => x.Resize(newW, newH));

                var outPath = GetOutputPath(item);
                image.Save(outPath);
            }
        }

        private static string GetOutputPath(FileInfo item) {
            return Path.Combine(item.DirectoryName, item.Name.Substring(0, item.Name.Length - item.Extension.Length) + "_resized" + item.Extension);
        }
    }
}
